<?php

//Route::get ('/register', 'RegistrationController@create');
//Route::post('/register','RegistrationController@store');



// Authentication Routes...
//Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
//Route::post('login', 'Auth\LoginController@login');
//Route::post('logout', 'Auth\LoginController@logout')->name('logout');
//// Password Reset Routes...
//Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
//Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
//Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
//Route::post('password/reset', 'Auth\ResetPasswordController@reset');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {return view('welcome');});


Route::get('/', 'UsersController@count');
Route::post('/','UsersController@contact_us');
Route::post('/notification_for_users','UsersController@notifications_for_users');

// Home Routes ...
Route::get('/home', 'UsersController@index')->name('home')->middleware(['is_user', 'is_admin']);

// Users Routes ...
Route::get('/users/registration', 'Auth\RegisterController@registration')->name('registration');
Route::post('/register', 'Auth\RegisterController@register');

Route::get('/users/create', 'UsersController@create')->name('create_newUser')->middleware(['is_user','is_policeStation']);
Route::post('/users', 'UsersController@store');
Route::get('/users/{user}', 'UsersController@show')->name('profile');
Route::post('/users/{id}','BicyclesController@store');
Route::post('users/stolenbicycles/{bicycle_id}', 'BicyclesController@steal_report');

Route::get('/show/message/{message}', 'UsersController@show_message')->name('show_message')->middleware('is_superAdmin');
Route::get('/show/stolenbicycles/{steal_report}', 'UsersController@show_steal_report')->name('show_steal_report');
Route::get('/markAsRead','UsersController@markAsRead')->name('read');

// Admins Routes
Route::get('admins/admin_create', 'UsersController@admin_create')->name('admin_create')->middleware('is_superAdmin');
Route::get('/admins/{admin_id}', 'UsersController@admin_dashboard')->name('admin_dashboard')->middleware('is_user');
Route::post('/admins', 'UsersController@admin_store');

//PoliceStation Routes
Route::get('create/police_station', 'UsersController@create_police_station')->name('create_police_station');
Route::post('police_stations', 'UsersController@police_station_store');

// Bicycles Routes ...
Route::get('/bicycles', 'BicyclesController@index')->name('bicycles')->middleware('is_user');
Route::get('/bicycles/create/{user}', 'BicyclesController@create')->name('create_bicycles')->where('user', '[0-9]+')->middleware('is_policeStation');
//Route::post('/bicycles/{user}', 'BicyclesController@store')->name('add_bicycle')->where('user', '[0-9]+');

Route::get('/search', 'BicyclesController@search')->name('search');
Route::get('/search/users', 'UsersController@index')->name('search/users');
Route::get('/bicycles/{id}', 'BicyclesController@info')->name('information');

Route::get('/bicycles/gallery/{bicycle}','BicyclesController@showGallery')->name('gallery');
Route::post('/bicycles/gallery/{bicycle}', 'BicyclesController@upload')->name('uploads')->middleware('is_policeStation');

// Edit Routes
Route::resource('bicycles', 'BicyclesController');
Route::resource('user', 'UsersController');


