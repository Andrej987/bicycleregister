@extends('app.app')
<link href="https://fonts.googleapis.com/css?family=Oleo+Script:400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Teko:400,700" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">


<section class=" pull-left" id="contact ">
<h1 class="left" style="margin-left: -300px">Kontaktirajte Nas</h1>
    <div class="contact-section">
        <div class="container bottom">
            <form method="POST" action="/">
                {{csrf_field()}}
                <div class="col-md-4 form-line">
                    <div class="form-group-sm">
                        <label for="exampleInputUsername">Ime</label>
                        <input type="text" class="form-control" id="contact_name" name="contact_name" placeholder=" Unesite Ime i Prezime">
                    </div>
                    <div class="form-group-sm">
                        <label for="exampleInputEmail">Email Adresa</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder=" Unesi Email ">
                    </div>
                    <div class="form-group-sm">
                        <label for="telephone">Broj Mobiela</label>
                        <input type="tel" class="form-control" id="mobile_phone_number" name="mobile_phone_number" placeholder=" Broj Mobitela">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="description"> Poruka ili upit</label>
                        <textarea style="width: 100%; height:144px " class="form-control" id="contact_message" name="contact_message" placeholder="Unesite tekst"></textarea>
                    </div>
                    <div>

                        <button type="submit" class="btn btn-default submit"><i class="fa fa-paper-plane"
                                                                                aria-hidden="true"></i> Pošalji
                        </button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</section>
