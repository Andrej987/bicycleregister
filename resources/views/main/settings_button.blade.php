<ul class="pull-right panel-settings panel-button-tab-right">
    <li class="dropdown"><a class="pull-right dropdown-toggle" data-toggle="dropdown" href="#">
            <em class="fa fa-cogs"></em>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            <li>
                <ul class="dropdown-settings">
                    <li><a href="#">
                            <em class="fa fa-cog"></em> Settings 1
                        </a></li>
                    <li class="divider"></li>
                    <li><a href="#">
                            <em class="fa fa-cog"></em> Settings 2
                        </a></li>
                    <li class="divider"></li>
                    <li><a href="#">
                            <em class="fa fa-cog"></em> Settings 3
                        </a></li>
                </ul>
            </li>
        </ul>
    </li>
</ul>

