<div class="panel-body">

    <ul class="todo-list">
        <li class="todo-list-item">
            <div class="checkbox">
                <input type="checkbox" id="checkbox-1" />
                <label for="checkbox-1">Make coffee</label>
            </div>
            <div class="pull-right action-buttons"><a href="#" class="trash">
                    <em class="fa fa-trash"></em>
                </a>
            </div>
        </li>
        <li class="todo-list-item">
            <div class="checkbox">
                <input type="checkbox" id="checkbox-2" />
                <label for="checkbox-2">Check emails</label>
            </div>
            <div class="pull-right action-buttons"><a href="#" class="trash">
                    <em class="fa fa-trash"></em>
                </a>
            </div>
        </ul>
</div>
