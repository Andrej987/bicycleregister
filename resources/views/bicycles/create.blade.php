@extends('app.app')

@section('content')


    <div>

        <div class="col-sm-6">

            <h1> Add New Bicycle </h1>

            <form method="POST" action="/bicycles/{{ $user->id }}">

                {{csrf_field()}}

                <div class="form-group">

                    <label for="name"> Bicycle Type: </label>

                    <input type="text" class="form-control" id="bicycle_type" name="bicycle_type" required>

                </div>

                <div class="form-group">

                    <label for="name"> Bicycle Model: </label>

                    <input type="text" class="form-control" id="bicycle_model" name="bicycle_model" required>

                </div>

                <div class="form-group">

                    <label for="name"> Color: </label>

                    <input type="text" class="form-control" id="color" name="color" required>

                </div>

                <div class="form-group">

                    <label for="name"> Frame Size: </label>

                    <input type="text" class="form-control" id="frame_size" name="frame_size" required>

                </div>

                <div class="form-group">

                    <label for="name"> Serial Number: </label>

                    <input type="number" class="form-control" id="serial_number" name="serial_number" required>

                </div>

                <div class="form-group">

                    <button type="submmit" class="btn-primary">Submit</button>

                </div>

            </form>

        </div>

    </div>

@endsection
