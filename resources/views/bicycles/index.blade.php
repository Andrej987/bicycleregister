@extends('app.app')

@section('content')

    @if(\Illuminate\Support\Facades\Auth::user())

        @include('sidebar')

    @endif

    <div class="col-md-6 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/home">
                        <em class="fa fa-home"></em>
                    </a></li>
                <li class="active">Lista registriranih bicikala</li>
            </ol>
        </div><!--/.row-->

        <table class="table">
            <thead>

            <tr>
                <div class="col-sm-11">
                    <th>Id</th>
                    <th>Tip Bicikla</th>
                    <th>Proizvođać</th>
                    <th>Model</th>
                    <th>Boja</th>
                    <th>Velićina Rame</th>
                    <th>Serijski Broj</th>
                    <th>Galerija Slika</th>
                </div>
            </tr>

            </thead>
            <tbody>

            @foreach($bicycles as $bicycle)
                <div class="table-responsive">
                    <td>{{$bicycle->id}}</td>
                    <td>{{$bicycle->bicycle_type}}</td>
                    <td>{{$bicycle->bicycle_manufacturer}}</td>
                    <td>{{$bicycle->bicycle_model}}</td>
                    <td>{{$bicycle->color}}</td>
                    <td>{{$bicycle->frame_size}}</td>
                    <td>
                        <a href="{{ route('information',['id'=>$bicycle->user_id])}}">
                            {{$bicycle->serial_number}}
                        </a>
                    </td>
                    <td>
                        @if($bicycle->featured_image)
                            <div class="profile-userpic">
                                <a href="{{ route('gallery',['id' => $bicycle->id])}}">
                                    <img src="{{ asset($bicycle->featured_image) }}" class="img-responsive" alt="">
                                </a>
                            </div>
                        @else()

                            <button class="img-circle" style="margin: 15%; border-radius: 80%  ">
                                <a href="{{ route('gallery',['id' => $bicycle->id])}}">
                                    <section class="fa fa-plus"></section>
                                </a>
                            </button>
                        @endif
                    </td>

                    @if(Auth::user())

                        <td><a href="{{action('BicyclesController@edit', $bicycle['id'])}}"
                               class="btn btn-warning">Edit</a></td>

                        <td>
                            <form action="{{action('BicyclesController@destroy', $bicycle['id'])}}" method="post">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>

                        @endif

                        </td>

                        </tr>

                </div>

            @endforeach

            </tbody>

        </table>

        {{$bicycles->links()}}

    </div>
@endsection
