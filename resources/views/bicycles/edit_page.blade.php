@extends('app.app')

@section('content')

@include('sidebar')


<div class="col-md-6 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/home">
                    <em class="fa fa-home"></em>
                </a></li>
        </ol>
    </div><!--/.row-->

    <table class="table">
        <thead>

        <tr>
            <div class="col-sm-11">
                <th>Id</th>
                <th>Proizvođać</th>
                <th>Tip Bicikla</th>
                <th>Model</th>
                <th>Boja</th>
                <th>Velićina Rame</th>
                <th>Serijski Broj</th>
            </div>
        </tr>

        </thead>
        <tbody>
        <tr>

            <form method="post" action="{{action('BicyclesController@update', $bicycle['id'])}}" class="btn btn-warning">

                        {!!csrf_field()!!}
                <input name="_method" type="hidden" value="PATCH">

                <td> {{$bicycle->id}} </td>
                <td> <input type="text" class="form-control" name="bicycle_manufacturer" value="{{$bicycle->bicycle_manufacturer}}"> </td>
                <td> <input type="text" class="form-control" name="bicycle_type" value="{{$bicycle->bicycle_type}}"> </td>
                <td> <input type="text" class="form-control" name="bicycle_model" value="{{$bicycle->bicycle_model}}"> </td>
                <td> <input type="text" class="form-control" name="color" value="{{$bicycle->color}}"> </td>
                <td> <input type="number" class="form-control" name="frame_size" value="{{$bicycle->frame_size}}"> </td>
                <td> <input type="text" class="form-control" name="serial_number" value="{{$bicycle->serial_number}}"> </td>

                <td>

                <div class="form-group">

                    <button type="submmit" class="btn-primary">Promjeni</button>

                </div>

                </td>

                </form>


        </tbody>

    </table>

</div>
</div>
@endsection


