@extends('app.app')
@section('content')
    @include('sidebar')

    <div class="col-md-6 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/home">
                        <em class="fa fa-home"></em>
                    </a></li>
                <li class="active">Galerija slika</li>
            </ol>
        </div><!--/.row-->


    <div class="container">


        <div class="tab-content">

            @if(\Illuminate\Support\Facades\Auth::check())

            <div class="navbar-right" width="100" height="100" >
                <form action="{{route('gallery',['bicycle' => $bicycle->id])}}" method="POST" enctype="multipart/form-data">
                    <input type="file" name="file" id="file" >
                    <input type="submit" value="Upload" name="submit">
                    <input type="hidden" value="{{csrf_token()}}" name="_token">
                </form>

            </div>
            @endif
            <card>

            @foreach($images as $image)
            <img src="{{asset($image->path)}}" class="img-rounded" height="350" width="350">
            @endforeach

            </card>

        </div>

    </div>

@endsection

