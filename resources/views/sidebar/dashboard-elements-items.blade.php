<ul class="nav menu">
    <li class=""><a href="{{route('home')}}"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>

    <li><a href="/bicycles"><em class="fa fa-bar-chart">&nbsp;</em> Bicycles</a></li>


    <li class="parent "><a data-toggle="collapse" href="#sub-item-1">
            <em class="fa fa-navicon">&nbsp;</em> Kreiraj <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
        </a>
        <ul class="children collapse" id="sub-item-1">

            <li><a href="{{ route('create_newUser') }}">
                    <span class="fa fa-arrow-right">&nbsp;</span> Korisnik </a>

               </li>

            <li> <a href="{{ route('admin_create') }}">
                    <span class="fa fa-arrow-right">&nbsp;</span> Admin </a>
                </li>

            <li> <a href="{{ route('create_police_station') }}">
                    <span class="fa fa-arrow-right">&nbsp;</span> Policijska Postaja </a>
                </li>

        </ul>
    </li>


    <li> <a href="{{ route('logout') }}"
           onclick="event.preventDefault();
         document.getElementById('logout-form').submit();">

            <em class="fa fa-power-off">&nbsp;</em>
            Logout
        </a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>

    </li>


</ul>

