<li class="parent "><a data-toggle="collapse" href="#sub-item-1">
        <em class="fa fa-navicon">&nbsp;</em> Multilevel <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
    </a>
    <ul class="children collapse" id="sub-item-1">
        <li><a class="" href="#">
                <span class="fa fa-arrow-right">&nbsp;</span> Sub Item 1
            </a></li>
        <li><a class="" href="#">
                <span class="fa fa-arrow-right">&nbsp;</span> Sub Item 2
            </a></li>
        <li><a class="" href="#">
                <span class="fa fa-arrow-right">&nbsp;</span> Sub Item 3
            </a></li>
    </ul>
</li>