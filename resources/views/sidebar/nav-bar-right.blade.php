<ul class="nav navbar-top-links navbar-right">
    <li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
            <em class="fa fa-envelope"></em><span class="label label-danger">15</span>
        </a>

        @include('layouts.sidebar.notifications.messages')

    </li>

        @include('layouts.sidebar.notifications.alerts')

    </ul>