<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Lumino - Dashboard</title>
    <link href="public/css/bootstrap.min.css" rel="stylesheet">
    <link href="public/css/font-awesome.min.css" rel="stylesheet">
    <link href="public/css/datepicker3.css" rel="stylesheet">
    <link href="public/css/styles.css" rel="stylesheet">
    <!--Custom Font-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="public/js/html5shiv.js"></script>
    <script src="public/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>

    @include('layouts.main.navigation-bar')

    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">

    @include('layouts.sidebar.profile-icon')

    <div class="divider"></div>

    @include('layouts.sidebar.search-form')

    @include('layouts.sidebar.dashboard-elements-items')

    </div><!--/.sidebar-->

    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#">
                    <em class="fa fa-home"></em>
                </a></li>
            <li class="active">Dashboard</li>
        </ol>
    </div><!--/.row-->

    {{--<div class="row">--}}
        {{--<div class="col-lg-12">--}}
            {{--<h1 class="page-header">Dashboard</h1>--}}
        {{--</div>--}}
    {{--</div><!--/.row-->--}}

   @include('layouts.main.counters')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                   Bicycle owners list :


            @include('layouts.main.settings_button')</div>

            @include('home')

{{--            @include('layouts.main.canvas')--}}

        </div>
    </div><!--/.row-->

{{--        @include('layouts.main.percentages')--}}

    {{--<div class="row">--}}
        {{--<div class="col-md-6">--}}
            {{--<div class="panel panel-default chat">--}}
                {{--<div class="panel-heading">--}}
                    {{--Chat--}}

                    {{--@include('layouts.main.settings_button')--}}

                            {{--@include('layouts.main.span_panel-button')--}}

                            {{--</div>--}}


                {{--@include('layouts.main.chat_container')--}}

             {{--@include('layouts.main.text-input_and_send-form')--}}

            {{--</div>--}}

            {{--<div class="panel panel-default">--}}

                {{--<div class="panel-heading">--}}

                    {{--To-do List--}}

                    {{--@include('layouts.main.settings_button')--}}

                    {{--@include('layouts.main.span_panel-button')--}}

                {{--</div>--}}

                {{--@include('layouts.main.tasks-lists-checkbox')--}}

                {{--@include('layouts.main.text-input_and_send-form')--}}

            {{--</div>--}}
        {{--</div><!--/.col-->--}}


        <div class="col-md-12">
            <div class="panel panel-default ">
                <div class="panel-heading">
                    Timeline

                    @include('layouts.main.settings_button')

                    @include('layouts.main.span_panel-button')<em class="fa fa-toggle-up"></em>

                </div>

                @include('layouts.main.timeline_container')

            </div>
        </div><!--/.col-->



        <div class="col-sm-12">
            <p class="back-link"> MarrowLabs </p>
        </div>
    </div><!--/.row-->
</div>	<!--/.main-->

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/chart.min.js"></script>
<script src="js/chart-data.js"></script>
<script src="js/easypiechart.js"></script>
<script src="js/easypiechart-data.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/custom.js"></script>
<script>
    window.onload = function () {
        var chart1 = document.getElementById("line-chart").getContext("2d");
        window.myLine = new Chart(chart1).Line(lineChartData, {
            responsive: true,
            scaleLineColor: "rgba(0,0,0,.2)",
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleFontColor: "#c5c7cc"
        });
    };
</script>
</div>
</body>
</html>