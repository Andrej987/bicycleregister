<button href="#" class="pull-right" id="add_new_bicycle"
        data-toggle="modal"
        data-target="#myModal"><i class="fa fa-plus"
                                  aria-hidden="true"></i></button>
<div class="panel-title">

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        {{csrf_field()}}
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="title">Add New Bicycle</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id">
                    <p><input type="text" placeholder="Bicycle Type" id="bicycle_type" class="form-control"></p>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id">
                    <p><input type="text" placeholder="Color" id="add_color" class="form-control"></p>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id">
                    <p><input type="text" placeholder="Frame Size" id="frame_size" class="form-control"></p>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id">
                    <p><input type="text" placeholder="Serial Number" id="serial_number" class="form-control">
                    </p>
                </div>
                <button type="button" class="btn btn-primary pull-right" id="add_button" data-dismiss="modal">
                    Add Bicycle
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->