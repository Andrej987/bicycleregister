@extends('app.app')

@section('content')

    @include('sidebar')

    <div class="center-block" id="contact ">
{{--        @foreach($messages as $message)--}}
            <div class="contact-section ">
                <div class="container bottom">
                    <div class="col-md-4 form-line">

                        <div class="form-group-sm">
                            <label for="exampleInputUsername"> Name </label>
                            <div type="text" class="form-control" id="contact_name" name="contact_name">
                                 {{$message->contact_name}}

                            </div>
                            <div class="form-group-sm">
                                <label for="exampleInputEmail">Email Address</label>
                                <div type="email" class="form-control" id="email" name="email">
                                   {{$message->email}}
                                </div>
                                <div class="form-group-sm">
                                    <label for="telephone">Mobile Phone Number </label>
                                    <div type="tel" class="form-control" id="mobile_phone_number"
                                         name="mobile_phone_number">
                                        {{$message->mobile_phone_number}}

                                    </div>
                                </div>
                                <div class="form-group-lg">
                                    <div class="form-group">
                                        <label for="description"> Message </label>
                                        <textarea class="form-control" style="width: 1000px; height: 200px;"
                                                  id="contact_message" name="contact_message"
                                        >  {{$message->contact_message}} </textarea>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        {{--@endforeach--}}
    </div>
@endsection