<div class="panel-title">

    <div class="modal fade" id="myModal_bicycle_information_modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">

            <div class="modal-content">

                <div class="modal-body"> Tip bicikla
                    <p><div class="form-control">
                        {{$bicycle->bicycle_type}}
                    </div>
                </div>
                <div class="modal-body"> Model bicikla
                    <p><div class="form-control">
                        {{$bicycle->bicycle_model}}
                    </div>
                </div>
                <div class="modal-body"> Boja
                    <p><div class="form-control">
                        {{$bicycle->color}}
                    </div>
                </div>
                <div class="modal-body"> Velićina rame
                    <p><div class="form-control">
                        {{$bicycle->frame_size}}
                    </div>
                </div>
                <div class="modal-body"> Serijski broj
                    <p><div class="form-control">
                        {{$bicycle->serial_number}}
                    </div>
                </div>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
