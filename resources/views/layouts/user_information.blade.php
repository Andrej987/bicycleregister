<div class="panel panel-default">
    <div class="panel-heading">
        Informacije o korisniku

        <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span>
    </div>
    <div class="panel-body">
        <ul class="todo-list">
            <li class="todo-list-item">
                {{$user->name}}
            </li>
            <li class="todo-list-item">
                {{$user->surname}}
            </li>
            <li class="todo-list-item">
                {{$user->email}}
            </li>
            <li class="todo-list-item">
                {{$user->address}}
            </li>
            <li class="todo-list-item">
                {{$user->residence}}
            </li>
            <li class="todo-list-item">
                {{$user->phone_number}}
            </li>
            <li class="todo-list-item">
            {{ Carbon\Carbon::parse($user->birth_date)->format('d/m/Y')}}
            </li>
            <li class="todo-list-item">
                {{$user->OIB}}
            </li>

        </ul>
    </div>
</div>