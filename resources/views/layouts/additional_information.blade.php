@extends('app.app')

@section('content')

    @if(\Illuminate\Support\Facades\Auth::user())

        @include('sidebar')

        @endif

    <div class="col-md-6 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/home">
                        <em class="fa fa-home"></em>
                    </a></li>
                <li class="active"> {{$user->name}} {{$user->surname}}</li>
            </ol>
        </div><!--/.row-->

        <div class="panel panel-default">
            <div class="panel-heading">
                Informacije o korisniku

                <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
            <div class="panel-body">
                <ul class="todo-list">
                    <li class="todo-list-item">
                        {{$user->name}}
                    </li>
                    <li class="todo-list-item">
                        {{$user->surname}}
                    </li>
                    <li class="todo-list-item">
                        {{$user->email}}
                    </li>
                    <li class="todo-list-item">
                        {{$user->residence}}
                    </li>
                    <li class="todo-list-item">
                        {{$user->birth_date}}
                    </li>
                    <li class="todo-list-item">
                        {{$user->OIB}}
                    </li>

                </ul>
            </div>
        </div>

            <table class="table">
                <thead>

                <tr>
                    <div class="col-sm-11">
                        <th>Id</th>
                        <th>Bicycle Type</th>
                        <th>Color</th>
                        <th>Frame Size</th>
                        <th>Serial Number</th>
                        <th>Gallery</th>
                    </div>
                </tr>

                </thead>
                <tbody>

                @foreach($user->bicycles as $bicycle)
                    <tr>

                        {{--<td><img src="{{asset($bicycle->featured_image) }}"/>resize(200, 200)</td>--}}
                        <td>{{$bicycle->id}}</td>
                        <td>{{$bicycle->bicycle_type}}</td>
                        <td>{{$bicycle->color}}</td>
                        <td>{{$bicycle->frame_size}}</td>
                        <td>{{$bicycle->serial_number}}</td>
                        <td>
                            <div class="profile-userpic">

                                <a href="{{ route('gallery',['id' => $bicycle->id])}}">

                                    <img src="{{ asset($bicycle->featured_image) }}" class="img-responsive" alt="">

                                </a>
                            </div>
                        </td>


                    </tr>
                @endforeach


                </tbody>

            </table>

        </div>
    </div>
    </div>
@endsection
