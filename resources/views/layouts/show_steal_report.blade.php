@extends('app.app')

@section('content')

    @include('sidebar')

    <div class="center-block" id="contact ">
        <div class="contact-section ">
            <div class="container bottom">

                <span class="fa-stack fa-lg pull-right"
                     href="#" class="pull-right" id="user_information_modal"
                    data-toggle="modal"
                    data-target="#myModal_user_information_modal">
                <i class="fa fa-circle-thin fa-stack-2x" style="color:blue"></i><i class="fa fa-info fa-stack-1x" style="color: blue"></i>
                </span>
                <span class="fa-stack fa-lg pull-right"
                      href="#" class="pull-right" id="user_information_modal"
                      data-toggle="modal"
                      data-target="#myModal_bicycle_information_modal">
                <i class="fa fa-circle-thin fa-stack-2x" style="color: blue"></i><i class="fa fa-bicycle fa-stack-1x" style="color:blue"></i>
                </span>

                @include('layouts.user_information_modal')
                @include('layouts.bicycle_information_modal')


                <div class="col-md-5 form-line">
                    <div class="form-group-sm">
                        <label for="exampleInputUsername"> Informacije o biciklu </label>
                        <div type="text" class="form-control" id="bicycle_information"
                             name="bicycle_inforamtion">{{$steal_report->bicycle_information}}
                        </div>
                        <div class="form-group-sm">
                            <label for="exampleInputEmail"> Datum Nestanka </label>
                            <div type="date" class="form-control" id="owner_information"
                                 name="owner_information">{{$steal_report->steal_date}}

                            </div>
                            <div class="form-group-sm">
                                <label for="telephone">Mjesto nestanka</label>
                                <div type="text" class="form-control" id="steal_location"
                                     name="steal_location">{{$steal_report->steal_location}}
                                </div>
                            </div>
                            <div class="form-group-lg">
                                <div class="form-group">
                                    <label for="description"> Kratak opis </label>
                                    <textarea class="form-control" style="width: 1000px; height: 200px;"
                                              id="contact_message" name="contact_message" placeholder=" "> {{$steal_report->steal_description}}

                                        </textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            {{--@endforeach--}}
        </div>


@endsection
