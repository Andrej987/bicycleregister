<div class="panel-title">

    <div class="modal fade" id="myModal_user_information_modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">

            <div class="modal-content">

                <div class="modal-body"> Ime
                    <p><div class="form-control">
                       {{$user->name}}
                    </div>
                </div>
                <div class="modal-body"> Prezime
                    <p><div class="form-control">
                        {{$user->surname}}
                    </div>
                </div>
                <div class="modal-body"> Email
                    <p><div class="form-control">
                        {{$user->email}}
                    </div>
                </div>
                <div class="modal-body"> Mjesto stanovanja
                    <p><div class="form-control">
                        {{$user->residence}}
                    </div>
                </div>
                <div class="modal-body"> Datum Rođenja
                    <p><div class="form-control">
                        {{$user->birth_date}}
                    </div>
                </div>
                <div class="modal-body"> OIB
                    <p><div class="form-control">
                        {{$user->OIB}}
                    </div>
                </div>
                <button class="btn btn-danger pull-right">
                <a href="{{route('profile',['id'=>$user->id])}}" style="color: black"> <em class="fa fa-reply " style="color: black"></em> Idi na profil</a>
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
