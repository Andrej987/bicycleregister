<div class="panel-title">

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">

            <form class="modal-content">
                {{csrf_field()}}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="add_new_bicycle_title">Add New Bicycle</h4>
                </div>
                <div class="modal-body">
                    <p><input type="hidden" placeholder="" id="user_id" value="{{$user->id}}"
                              class="form-control">
                </div>

                <div class="modal-body"> Izaberite tip bicikla:

                    <select name="bicycle_type">

                        <option type="text" id="bicycle_type" value="MountainBike">Moutain Bike (MBT)</option>
                        <option type="text" id="bicycle_type" value="Cestovni">Cestovni</option>
                        <option type="text" id="bicycle_type" value="Trekking/cross">Trekking/Cross</option>
                        <option type="text" id="bicycle_type" value="Gradski">Gradski</option>
                        <option type="text" id="bicycle_type" value="Cruiser">Cruiser</option>
                        <option type="text" id="bicycle_type" value="Sklopivi">Sklopivi</option>
                        <option type="text" id="bicycle_type" value="Elektricni">Elektricni</option>
                        <option type="text" id="bicycle_type" value="Tricikl">Tricikl</option>


                    </select>

                </div>

                <div class="modal-body">
                    <p><input type="text" placeholder="Proizvođac" id="bicycle_manufacturer" class="form-control"></p>
                </div>
                <div class="modal-body">
                    <p><input type="text" placeholder="Model" id="bicycle_model" class="form-control"></p>
                </div>
                <div class="modal-body">
                    <p><input type="text" placeholder="Boja" id="color" class="form-control"></p>
                </div>
                <div class="modal-body">
                    <p><input type="number" placeholder="Velicina Rame" id="frame_size" class="form-control"></p>
                </div>
                <div class="modal-body">
                    <p><input type="text" placeholder="Serijski Broj" id="serial_number" class="form-control">
                    </p>
                </div>
                <div class="modal-body">
                    <p><input type="hidden" placeholder="Prijavljena Krađa" id="is_steal" value="" class="form-control">
                    </p>
                </div>
                <button type="button" class="btn btn-primary pull-right" id="add_button" data-dismiss="modal">
                    Add Bicycle
                </button>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
