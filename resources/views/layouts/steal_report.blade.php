<div class="panel-title">

    <div class="modal fade" id="steal_report" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">

            <form class="modal-content">
                {{csrf_field()}}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="steal_report_title">Prijava krađe</h4>
                </div>
                <div class="modal-body">
                    <p><input type="hidden" placeholder="" id="user_id" value="{{$user->id}}"
                              class="form-control">
                </div>
                <div class="modal-body">
                    <p><input type="hidden" placeholder="" id="bicycle_id" value="{{$bicycle->id}}"
                              class="form-control">
                </div>
                <div class="modal-body"> Informacije o biciklu i vlasniku
                    <p><input type="text"
                              placeholder="{{$bicycle->bicycle_manufacturer}}{{$bicycle->bicycle_model}} {{$bicycle->serial_number}} / {{$user->name}}  {{$user->surname}} , {{$user->residence}}"
                              value="{{$bicycle->bicycle_manufacturer}} {{$bicycle->bicycle_model}} - {{$bicycle->serial_number}} / {{$user->name}}  {{$user->surname}} , {{$user->residence}}"
                              id="bicycle_information" class="form-control"></p>
                </div>
                <div class="modal-body"> Datum nestanka
                    <p><input type="date" placeholder="" id="steal_date" class="form-control"></p>
                </div>
                <div class="modal-body"> Mjesto nestanka
                    <p><input type="text" placeholder="" id="steal_location" class="form-control"></p>
                </div>
                <div class="modal-body"> Opis ( Lokacija nestanka, obilježja bicikla )
                    <p><textarea type="text" style="width:100%; height:200px" placeholder="Kratak opis"
                                 id="steal_description" class="form-control"></textarea>
                    </p>
                </div>
                <div class="modal-body">
                    <p><input type="hidden" placeholder="is_read" id="is_read" value="" class="form-control">
                    </p>
                </div>
                <button type="button" class="btn btn-danger pull-right" id="send_steal_report"
                        data-dismiss="modal">
                    Send Steal Report
                </button>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->