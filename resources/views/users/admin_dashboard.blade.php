@extends('app.app')

@section('content')

@include('sidebar')


    <div class="col-md-6 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/home">
                        <em class="fa fa-home"></em>
                    </a></li>
                <li class="active">Korisnici</li>
            </ol>
        </div><!--/.row-->

        <tr class="row">

            <table class="table">
                <thead>

                <tr>

                </tr>

                <tr>
                    <div class="col-sm-11">
                        <th>Id</th>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Residence</th>
                        <th>Birth Date</th>
                        <th>OIB</th>
                    </div>
                </tr>

                </thead>
                <tbody>

                @foreach($users as $user)
                    <tr>

                        {{--<td><img src="{{asset($bicycle->featured_image) }}"/>resize(200, 200)</td>--}}
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->surname}}</td>
                        <td>{{$user->residence}}</td>
                        <td>{{$user->birth_date}}</td>
                        <td>
                            <a href="{{ route('profile',['id' => $user->id])}}">
                                {{$user->OIB}}
                            </a>
                        </td>

                        <td><a href="{{action('UsersController@edit', $user['id'])}}" class="btn btn-warning">Edit</a></td>

                        <td>
                            <form action="{{action('UsersController@destroy', $user['id'])}}" method="post">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>

                    </tr>
                @endforeach


                </tbody>

            </table>

    </div>
    </div>


@endsection