@extends('app.app')

@section('content')


    <div>

        <div class="col-sm-5">

            <h1> Create New Admin </h1>

            <form method="POST" action="/admins">

             {{csrf_field()}}

                <div class="form-group">

                    <label for="name"> Name: </label>

                    <input type="text" class="form-control" id="admin_name" name="name" required>

                </div>

                <div class="form-group">

                    <label for="name"> Address: </label>

                    <input type="text" class="form-control" id="address" name="address" required>

                </div>

                <div class="form-group">

                    <label for="name"> Residence: </label>

                    <input type="text" class="form-control" id="residence" name="residence" required>

                </div>

                <div class="form-group">

                    <label for="email"> Email: </label>

                    <input type="email" class="form-control" id="email" name="email" required>

                </div>

                <div class="form-group">

                    <label for="email"> Password: </label>

                    <input type="password" class="form-control" id="password" name="password" required>

                </div>

                <div class="form-group">

                    <button type="submmit" class="btn-primary">Create</button>

                </div>

            </form>

        </div>

    </div>

@endsection