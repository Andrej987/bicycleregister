@extends('app.app')

@section('content')


    <div class="col-sm-4">

        <title> New User Register </title>

        <form method="POST" action="/register">

            {{csrf_field()}}


            <div class="panel panel-default">
                <div class="panel-heading">
                    Registracija
                </div>

            </div>

            <div class="form-group-sm">

                <label for="name"> Izaberite najbližu policijsku postaju : </label>

                    <select name="admin_id">
                        @foreach($users as $user)
                        <option type="number" id="admin_id" name="admin_id"
                                value={{$user->id}}>{{$user->residence}} </option>
                        @endforeach
                    </select>

            </div>


            <div class="form-group-sm">

                <label for="name"> Name: </label>

                <input type="text" class="form-control" id="name" name="name" required>

            </div>

            <div class="form-group-sm">

                <label for="name"> Surname: </label>

                <input type="text" class="form-control" id="surname" name="surname" required>

            </div>

            <div class="form-group-sm">

                <label for="email"> Email: </label>

                <input type="email" class="form-control" id="email" name="email" required>

            </div>

            <div class="form-group-sm">

                <label for="password"> Password: </label>

                <input type="password" class="form-control" id="password" name="password" required>

            </div>

            <div class="form-group-sm">

                <label for="name"> Residence: </label>

                <input type="text" class="form-control" id="residence" name="residence" required>

            </div>

            <div class="form-group-sm">

                <label for="name"> Address: </label>

                <input type="text" class="form-control" id="address" name="address" required>

            </div>

            <div class="form-group-sm">

                <label for="name"> Phone Number: </label>

                <input type="number" class="form-control" id="phone_number" name="phone_number" required>

            </div>

            <div class="form-group-sm">

                <label for="name"> Birth Date: </label>

                <input  type="date"  class="form-control" id="birth_date" name="birth_date" required>

            </div>

            <div class="form-group-sm">

                <label for="name"> OIB: </label>

                <input type="number" class="form-control" id="OIB" name="OIB" required>

            </div>

            <div class="form-group">

                <button type="submmit" class="btn-primary">Register</button>

            </div>

        </form>

    </div>

@endsection