@extends('app.app')

@section('content')

@include('sidebar')


    <div class="col-md-6 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/home">
                        <em class="fa fa-home"></em>
                    </a></li>
                <li class="active"> {{$user->name}} {{$user->surname}}</li>
            </ol>
        </div><!--/.row-->

        @include('layouts.user_information')

        <div class="panel-heading">
            Registrirani Bicikli


        </div>

        <button style="margin-left: 93%"  href="#" class="btn btn-info" id="add_new_bicycle"
                data-toggle="modal"
                data-target="#myModal"><i class="fa fa-plus"
                                          aria-hidden="true"></i> Dodaj
        </button>

        @include('layouts.add_bicycle')

        <table class="table" id="bicycles">
            <thead>

            <tr>
                <div class="col-sm-11">
                    <th>Id</th>
                    <th>Tip Bicikla</th>
                    <th>Proizvođać Bicikla</th>
                    <th>Boja</th>
                    <th>Veličina Rame</th>
                    <th>Serijski Broj </th>
                    <th>Galerija Slika</th>
                    <th></th>
                    <th></th>
                    <th class="text-center"> Prijava Krađe </th>
                </div>
            </tr>

            </thead>
            <tbody>

            @foreach($user->bicycles as $bicycle)
                <tr>
                    <td>{{$bicycle->id}}</td>
                    <td>{{$bicycle->bicycle_type}}</td>
                    <td>{{$bicycle->bicycle_manufacturer}}</td>
                    <td>{{$bicycle->color}}</td>
                    <td>{{$bicycle->frame_size}}</td>
                    <td>{{$bicycle->serial_number}}</td>
                    <td>

                        @if($bicycle->featured_image)
                            <div class="profile-userpic">

                                <a href="{{ route('gallery',['id' => $bicycle->id])}}">

                                    <img src="{{ asset($bicycle->featured_image) }}" class="img-responsive" alt="">

                                </a>
                            </div>
                        @else()
                            <button class="img-circle" style="margin: 15%; border-radius: 80%  ">

                                <a href="{{ route('gallery',['id' => $bicycle->id])}}">

                                    <section class="fa fa-plus"></section>

                                </a>
                            </button>
                        @endif
                    </td>

                    <td>

                        <a href="{{action('BicyclesController@edit', $bicycle['id'])}}"
                           class="btn btn-warning">Edit</a>

                    </td>
                    <td>
                        <form action="{{action('BicyclesController@destroy', $bicycle['id'])}}" method="post">
                            {{csrf_field()}}
                            <input name="_method" type="hidden" value="DELETE">
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </td>

                    <td>
                        <button href="#" class="center-block" id="create_steal_report"
                                data-toggle="modal"
                                data-target="#steal_report"><i class="fa fa-exclamation"
                                                               aria-hidden="true"></i></button>
                    </td>

                </tr>
            @endforeach
            </tbody>

        </table>

        @include('layouts.steal_report')

        <script
                src="https://code.jquery.com/jquery-1.11.1.min.js"
                integrity="sha256-VAvG3sHdS5LqTT+5A/aeq/bZGa/Uj04xKxY8KM/w9EE="
                crossorigin="anonymous"></script>

        <script>
            $(document).ready(function (event) {

                $('#add_button').click(function (event) {
                    var user_id = $('#user_id').val();
                    var bicycle_type = $('#bicycle_type').val();
                    var bicycle_manufacturer = $('#bicycle_manufacturer').val();
                    var bicycle_model = $('#bicycle_model').val();
                    var color = $('#color').val();
                    var frame_size = $('#frame_size').val();
                    var serial_number = $('#serial_number').val();
                    var is_steal = $('#is_steal').val();

                    if (bicycle_manufacturer == "") {
                        alert('Bicycle Manufacturer is required');
                    }
                    if (bicycle_type == "") {
                        alert('Bicycle Type is required');
                    }
                    if (bicycle_model == "") {
                        alert('Bicycle Model is required');
                    }
                    if (bicycle_type < '5') {
                        alert('Bicycle Type must be minimum 5 characters. Please check your input. ');
                    }
                    if (color == "") {
                        alert('Color is required, please enter.');
                    }
                    if (frame_size == "") {
                        alert('Frame Size is required, please make input.');
                    }
                    if (serial_number == "") {
                        alert('Serial Number is required');
                    }
                    $.post('', {
                        'user_id': user_id,
                        'bicycle_manufacturer': bicycle_manufacturer,
                        'bicycle_type': bicycle_type,
                        'bicycle_model': bicycle_model,
                        'color': color,
                        'frame_size': frame_size,
                        'serial_number': serial_number,
                        'is_steal': is_steal => 0,
                        '_token': $('input[name=_token]').val()
                    }, function (data) {
                        window.location.reload();
                        console.log(data);

                    });
                });
            });
        </script>

        <script>
            $(document).ready(function (event) {

                $('#send_steal_report').click(function (event) {
                    var user_id = $('#user_id').val();
                    var bicycle_id = $('#bicycle_id').val();
                    var bicycle_information = $('#bicycle_information').val();
                    var steal_date = $('#steal_date').val();
                    var steal_location = $('#steal_location').val();
                    var steal_description = $('#steal_description').val();
                    var is_read = $('#is_read').val();

                    $.post('stolenbicycles/{{$bicycle->id}}', {
                        'user_id': user_id,
                        'bicycle_id': bicycle_id,
                        'bicycle_information': bicycle_information,
                        'steal_date': steal_date,
                        'steal_location': steal_location,
                        'steal_description': steal_description,
                        'is_read': is_read => 0,
                        '_token': $('input[name=_token]').val()
                    }, function (data) {
                        window.location.reload();
                        console.log(data);
                    });
                });
            });

        </script>
    </div>
    </div>
    </div>
@endsection







