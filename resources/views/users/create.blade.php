@extends('app.app')

@section('content')

    <div>

        <div class="col-sm-4">

            <form method="POST" action="/users">

                {{csrf_field()}}


                <div class="form-group-sm">

                    <label for="name"> Name: </label>

                    <input type="text" class="form-control" id="name" name="name" required>

                </div>

                <br>
                <div class="form-group-sm">

                    <label for="name"> Surname: </label>

                    <input type="text" class="form-control" id="surname" name="surname" required>

                </div>
                <br>

                <div class="form-group-sm">

                    <label for="email"> Email: </label>

                    <input type="email" class="form-control" id="email" name="email" required>

                </div>
                <br>

                <div class="form-group-sm">

                    <label for="password"> Password: </label>

                    <input type="password" class="form-control" id="password" name="password" required>

                </div>
                <br>

                <div class="form-group-sm">

                    <label for="name"> Residence: </label>

                    <input type="text" class="form-control" id="residence" name="residence" required>

                </div>
                <br>

                <div class="form-group-sm">

                    <label for="name"> Address: </label>

                    <input type="text" class="form-control" id="address" name="address" required>

                </div>
                <br>

                <div class="form-group-sm">

                    <label for="name"> Phone Number: </label>

                    <input type="tel" class="form-control" id="phone_number" name="phone_number" required>

                </div>
                <br>
                <div class="form-group-sm">

                    <label for="name"> Birth Date: </label>

                    <input type="date" class="form-control" id="birth_date" name="birth_date" required>

                </div>
                <br>

                <div class="form-group-sm">

                    <label for="name"> OIB: </label>

                    <input type="number" class="form-control" id="OIB" name="OIB" required>

                </div>
                <br>

                <div class="form-group">

                    <button type="submmit" class="btn-primary">Register</button>

                </div>

            </form>

        </div>


    </div>

@endsection