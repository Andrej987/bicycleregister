@extends('app.app')

@section('content')

    @include('sidebar')


    <div class="col-md-6 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/home">
                        <em class="fa fa-home"></em>
                    </a></li>
                {{--<li class="active"> {{$user->name}} {{$user->surname}}</li>--}}
            </ol>
        </div><!--/.row-->

        <table class="table">
            <thead>

            <tr>
                <div class="col-sm-11">
                    <th>Id</th>
                    <th>Admin Id</th>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Email</th>
                    <th>Residence</th>
                    {{--<th>Gallery</th>--}}
                </div>
            </tr>

            </thead>
            <tbody>

            {{--        @foreach($user->bicycles as $bicycle)--}}
            <tr>

                <form method="post" action="{{action('UsersController@update', $user['id'])}}" class="btn btn-warning">

                    {!!csrf_field()!!}
                    <input name="_method" type="hidden" value="PATCH">

                    <td> {{$user->id}} </td>
                    <td>
                        <ul class="select-box">

                            <select name="admin_id">

                                @foreach($admins as $admin)
                                    <option type="number" id="admin_id" name="admin_id" value="{{$user->admin_id}}">{{$admin->residence}} </option>
                                @endforeach
                            </select>


                        </ul>
                    </td>
                    {{--<td> <input type="number" class="form-control" name="admin_id" value={{$user->admin_id}}>  </td>--}}
                    <td> <input type="text" class="form-control" name="name" value="{{$user->name}}"> </td>
                    <td> <input type="text" class="form-control" name="surname" value="{{$user->surname}}"> </td>
                    <td> <input type="email" class="form-control" name="email" value="{{$user->email}}"> </td>
                    <td> <input type="text" class="form-control" name="residence" value="{{$user->residence}}"> </td>

                    <td>

                        <div class="form-group">

                            <button type="submmit" class="btn-primary">Promjeni</button>

                        </div>

                    </td>

                </form>


            </tbody>

        </table>

    </div>
    </div>
@endsection
