@extends('app.app')

@section('content')

    @include('sidebar')

    <div class="col-md-6 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">

        @if(Auth::user()->role_id == 3)
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="/home">
                            <em class="fa fa-home"></em>
                        </a></li>
                    <li class="active">Korisnici</li>

                    <li class="parent " data-toggle="collapse" href="#sub-item-2">
                        <em class="fa fa-toggle-down">&nbsp;</em>Adminske Liste / <span data-toggle="collapse"
                                                                                        href="#sub-item-2"
                                                                                        class="icon pull-right"></span>
                    </li>
                    <ol class="children collapse" id="sub-item-2">
                        @foreach($admins as $user)
                            <li>
                                <a href="{{route('admin_dashboard',['id'=>$user->id])}}">
                                    {{$user->residence}}</a>

                            </li>
                        @endforeach
                    </ol>

                    <li class="parent " data-toggle="collapse" href="#notifications">
                        <em class="fa fa-bell">&nbsp;</em> Napomene <span data-toggle="collapse"
                                                                          href="#sub-item-3"
                                                                          class="label label-info"
                                                                          style="background-color: red">{{$new_notifications}}</span>
                    </li>


                    <ul class="children collapse" id="notifications">
                        @foreach($notifications as $notification)
                            <li>
                                <a style="color: inherit"
                                   href="{{route('profile',['id'=>$notification->user_id])}}">{{$notification->text}}
                                </a>

                            </li>
                        @endforeach


                        <div class="pull-down action-buttons"><a href="/markAsRead" class="checkbox">
                                <button class="button btn-success"> Procitano</button>
                            </a>
                        </div>


                    </ul>

                    <li class="parent " data-toggle="collapse" href="#message">
                        / <em class="fa fa-envelope">&nbsp;</em> Nove Poruke <span data-toggle="collapse"
                                                                                   href="#sub-item-3"
                                                                                   class="label label-info"
                                                                                   style="background-color: red">{{$new_messages}}</span>
                    </li>

                    <ul class="children collapse" id="message">
                        @foreach($messages as $notification)

                            <li>
                                <a style="color: inherit"
                                   href="{{route('show_message',['message'=>$notification->message_id])}}">{{$notification->text}}
                                </a>
                            </li>
                        @endforeach

                    </ul>

                    <li class="parent " data-toggle="collapse" href="#steal_report">
                        / <em class="fa fa-warning">&nbsp;</em> Prijavljene krađe <span data-toggle="collapse"
                                                                                        href="#sub-item-3"
                                                                                        class="label label-info"
                                                                                        style="background-color: red">{{$new_steal_reports}}</span>
                    </li>

                    <ul class="children collapse" id="steal_report">
                        @foreach($steal_reports as $notification)

                            <li>
                                <a style="color: inherit"
                                   href="{{route('show_steal_report',['steal_report'=>$notification->steal_report_id])}}">{{$notification->text}}
                                </a>
                            </li>
                        @endforeach

                    </ul>

                    <button style="margin-left: 88.5%" href="#" class="btn btn-info" id="notification_to_users_button"
                            data-toggle="modal"
                            data-target="#users_notifications"> Obavijesti
                    </button>

                    <div class="panel-title">

                        <div class="modal fade" id="users_notifications" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">

                                <form class="modal-content">
                                    {{csrf_field()}}
                                    <div class="modal-body"> Obavijest Korisnicima
                                        <p><textarea type="text" style="width:100%; height:200px"
                                                     placeholder="Obavijest"
                                                     id="notification_for_users" class="form-control"></textarea>
                                        </p>
                                    </div>
                                    <button type="button" class="btn btn-danger pull-right"
                                            id="send_notification_to_users_button"
                                            data-dismiss="modal">
                                        Pošalji
                                    </button>
                                </form>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                </ol>
            </div><!--/.row-->
        @endif


        <table class="table">
            <thead>
            <tr>
                <div class="col-sm-11">
                    <th>Id</th>
                    <th>Ime</th>
                    <th>Prezime</th>
                    <th>Adresa</th>
                    <th>Mjesto Prebivališta</th>
                    <th>OIB</th>
                </div>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->surname}}</td>
                    <td> {{$user->address}}</td>
                    <td>{{$user->residence}}</td>
                    <td>
                        <a href="{{ route('profile',['id' => $user->id])}}">
                            {{$user->OIB}}
                        </a>
                    </td>
                    <td><a href="{{action('UsersController@edit', $user['id'])}}" class="btn btn-warning">Edit</a>
                    </td>
                    <td>
                        <form action="{{action('UsersController@destroy', $user['id'])}}" method="post">
                            {{csrf_field()}}
                            <input name="_method" type="hidden" value="DELETE">
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{$users->links()}}

        {{--<script--}}
                {{--src="https://code.jquery.com/jquery-1.11.1.min.js"--}}
                {{--integrity="sha256-VAvG3sHdS5LqTT+5A/aeq/bZGa/Uj04xKxY8KM/w9EE="--}}
                {{--crossorigin="anonymous"></script>--}}

        {{--<script>--}}
            {{--$(document).ready(function (event) {--}}
                {{--$('#send_notification_to_users_button').click(function (event) {--}}
                    {{--var notification_for_users = $('#notification_for_users').val();--}}
                    {{--$.post('notification_for_users', {--}}
                        {{--'notification_for_users': notification_for_users--}}
                    {{--}, function (data) {--}}
                        {{--window.location.reload();--}}
                        {{--console.log(data);--}}
                    {{--});--}}
                {{--});--}}
            {{--});--}}
        {{--</script>--}}

    </div>
    </div>
@endsection



