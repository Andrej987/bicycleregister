<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bicycle Register</title>
    <link href="{{asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/datepicker3.css')}}" rel="stylesheet">
    <link href="{{asset('css/datepicker.css')}}" rel="stylesheet">
    <link href="{{asset('css/styles.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootstrap-theme.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootstrap-table.css')}}" rel="stylesheet">
    <!--Custom Font-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i"
          rel="stylesheet">
    {{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}
    {{--<script src="{{asset('js/html5shiv.js')}}"></script>--}}
    <script src="{{asset('js/respond.min.js')}}"></script>
</head>
<!-- Styles -->
<style>
    html, body {
        background-color: #fff;
        color: #636b6f;
        font-family: 'Raleway', sans-serif;
        font-weight: 100;
        height: 100vh;
        margin: 0;
    }

    .full-height {
        height: 100vh;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

    .top-right {
        position: absolute;
        right: 10px;
        top: 5px;
    }

    .content {
        text-align: center;

    }

    .title {
        font-size: 50px;
    }

    .links > a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 12px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
    }

    .m-b-md {

        background-image: url("http://www.minnesotainjury.com/wp-content/uploads/2017/03/bigstock-146723681.jpg");
    }


</style>
</head>
<body>
<div class="m-b-md position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
            <a href="{{ url('/home') }}" href="/home">
                <button style="border-radius: 100%"><em class="fa fa-home" style="width:17px; height: 17px;"> </em>
                </button>
            </a>
            @else
                <a href="{{ route('login') }}">Login</a>
                <a href="{{ route('registration') }}">Register</a>
                @endauth


        </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            <B>Dobrodošli u</B>
            <br> Evidenciju Bicikala</br>
        </div>

        <div class="panel panel-container" style="background: transparent">  </div>
            <div class="col-xs-6 col-md-3 col-lg-3 " style="background: transparent">
                        <div class="row no-padding " style="background: transparent"><em class="fa fa-xl fa-shopping-cart color-blue"></em>
                            <div class="large">120</div>
                            <div class="text-muted">Broj gradova</div>
                        </div>
            </div>

                <div class="col-xs-6 col-md-3 col-lg-3 no-padding">
                        <div class="row no-padding" style="background: transparent"><em class="fa fa-xl fa-users color-teal"></em>
                            <div class="large">{{$all_users}}</div>
                            <div class="text-muted">Ukupan broj korisnika</div>
                        </div>
                </div>

                <div class="col-xs-6 col-md-3 col-lg-3 no-padding">
                        <div class="row no-padding" style="background: transparent"><em class="fa fa-xl fa-comments color-orange"></em>
                            <div class="large"> {{$all_bicycles}}  </div>
                            <div class="text-muted">Broj registriranih bicikala</div>
                    </div>
                </div>

                <div class="col-xs-6 col-md-3 col-lg-3 no-padding">
                        <div class="row no-padding" style="background: transparent"><em class="fa fa-xl fa-search color-red"></em>
                            <div class="large">25.2k</div>
                            <div class="text-muted">Broj PRETRAGA</div>
                    </div>
                </div>

        <div style="margin-top: 6%">  <strong style="margin-bottom:10%; color: black; "> Pretraži Bicikle </strong> </div>

        <header>
            <form class="fa fa-search" action="/search" method="GET" class="searchbar" autocomplete="off" role="search">
                <input name="search" type="text"  value="" tabindex="1" autocomplete="off"
                       maxlength="500" class="glyphicon-search" style="width: 400px; height: 35px; margin-top:1%; background-color:#faf7fe ">
            </form>
        </header>

        <div class="col-sm-2" style="margin-left:28%; margin-top: 11%">
            @include('main.contact_form')
        </div>
    </div>
</div>
</body>
</html>
