<?php

use App\Role;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

//        $role = Role::where('name', 'SuperAdmin')->first();
        // Insert some stuff
        DB::table('roles')->insert(
            array(
                'id' => '3',
                'name'=> 'superAdmin'

            )
        );

        DB::table('roles')->insert(
            array(
                'id' => '2',
                'name'=> 'admin'

            )
        );

        DB::table('roles')->insert(
            array(
                'id' => '1',
                'name'=> 'user'
            )
        );

        $role = Role::where('name', 'superAdmin')->first();
        // Insert some stuff
        DB::table('users')->insert(
            array(
                'role_id' => $role->id,
                'name' => 'Example',
                'surname' => 'Example',
                'email' => 'example@example.com',
                'password' => bcrypt('example'),
                'residence'=>'Donji Miholjac',
                'OIB'=>'9876543210',

            )
        );

        $role = Role::where('name', 'admin')->first();
        // Insert some stuff
        DB::table('users')->insert(
            array(
                'role_id' => $role->id,
                'name' => 'admin',
                'surname' => 'admin',
                'email' => 'admin@admin.com',
                'password' => bcrypt('admin'),
                'residence'=>'Radikovci',
                'OIB'=>'1234567890',

            )
        );

        $role = Role::where('name', 'user')->first();
        // Insert some stuff
        DB::table('users')->insert(
            array(
                'role_id' => $role->id,
                'name' => 'user',
                'surname' => 'user',
                'email' => 'user@user.com',
                'password' => bcrypt('user'),
                'residence'=>'Osijek',
                'OIB'=>'9871234560',

            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}