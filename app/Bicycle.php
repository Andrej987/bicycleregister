<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Bicycle extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'user_id','bicycle_type','bicycle_manufacturer','bicycle_model', 'color', 'frame_size', 'serial_number', 'is_steal'
    ];

    protected $table = 'bicycles';

    public function user(){

       return $this->belongsTo(User::class);

    }

    public function image(){

        return $this->hasMany(Image::class);

    }

    public function steal_report(){

        return $this->belongsTo(StealReport::class);

    }

    public function getFeaturedImageAttribute()
    {
        $db_image = Image::where('bicycle_id', $this->id)->first();
        if ($db_image)
        {
            return $db_image->path;
        }
    }
}
