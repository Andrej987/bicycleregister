<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Message extends Model
{
    protected $fillable = ['is_read','contact_name', 'email', 'mobile_phone_number', 'contact_message'];

    protected $table = 'messages';

//    public function user()
//    {
//        return $this->belongsTo(User::class);
//
//    }

    public function notification (){

        return $this->belongsTo(Notifications::class);

    }

}
