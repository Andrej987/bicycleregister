<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class StealReport extends Model
{
    use Notifiable;

    protected $fillable = [
        'user_id','bicycle_id','bicycle_information','steal_date','steal_location','steal_description','is_read'
    ];

    protected $table = 'steal_reports';


//    public function user(){
//
//        return $this->belongsTo(User::class);
//
//    }

    public function notification(){

        return $this->belongsTo(Notifications::class);

    }

    public function bicycle()
    {

        return $this->belongsTo(Bicycle::class);

    }
}
