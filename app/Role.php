<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
       'id', 'name'
    ];

    protected $table = 'roles';

    public function user(){

        return $this->hasMany(User::class);

    }
//
//    public static function admin(){
//
//        $admin = Role::where('name','admin')->first();
//        return $admin->hasMany(User::class);
//
//    }

}
