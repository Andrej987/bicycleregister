<?php

namespace App\Http\Middleware;

use App\Role;
use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;


class SuperAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle( $request, Closure $next)
    {
      $superAdmin_role = User::where('role_id','3')->first();

        if (Auth::user('role_id') == $superAdmin_role) {

            return $next($request);

        } else {

           return back();
        }
    }
}
