<?php

namespace App\Http\Middleware;

use App\Role;
use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle( $request , Closure $next)
    {

        $user_role = User::where('role_id','1')->first();

        if (Auth::user('role_id') == $user_role) {

          return back();

        } else {

            return $next($request);
        }

    }
}
