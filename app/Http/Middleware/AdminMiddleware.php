<?php

namespace App\Http\Middleware;

use App\Repositories\Roles;
use App\Role;
use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $admin_role = User::where('role_id','2')->first();

        if (Auth::user('role_id') == $admin_role) {

            return back();

        } else {

            return $next($request);
        }
    }
}

