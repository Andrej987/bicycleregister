<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class PoliceStationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle( $request, Closure $next)
    {
        $policeStation_role = User::where('role_id','4')->first();

        if (Auth::user('role_id') == $policeStation_role ) {

            return back();

        } else {

            return $next($request);
        }
    }
}
