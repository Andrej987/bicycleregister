<?php

namespace App\Http\Controllers;

use App\Bicycle;
use App\Events\StolenBicycleAlertEvent;
use App\Image;
use App\Repositories\Profiles;
use App\StealReport;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class BicyclesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('search', 'info', 'showGallery');
        $this->middleware('is_policeStation')->except('index', 'search', 'info', 'showGallery');
        $this->middleware('is_user')->only('index');


    }

    public function index(User $user)
    {

        $bicycle = Bicycle::where('user_id', $user->id)->first();
        $bicycles = Bicycle::all();

        $bicycles = Bicycle::paginate(8);

        return view('bicycles.index', compact('bicycles', 'user', 'images', 'bicycle'));

    }

    public function search(Request $request)
    {

        $search = $request->input('search');

        $query = Bicycle::query();

        if ($search) {
            $query->where('serial_number', 'like', '%' . $search . '%')
                ->orWhere('bicycle_type', 'like', '%' . $search . '%')
                ->orWhere('frame_size', 'like', '%' . $search . '%');
        }

        $bicycles = $query->paginate(10);

        return view('bicycles.index', compact('bicycles', 'user', 'images', 'bicycle'));

    }

    public function create($id)
    {

        $user = User::find($id);
        return view('bicycles.create', compact('user'));

    }

    public function store(Request $request)
    {

        Validator::make($request->all(), [
            'bicycle_type' => 'required|min:5',
            'bicycle_model' => 'required',
            'color' => 'required',
            'frame_size' => 'required',
            'serial_number' => 'required|unique:bicycles']);

        Bicycle::create([

            'user_id' => $request['user_id'],
            'bicycle_manufacturer'=>$request['bicycle_manufacturer'],
            'bicycle_type' => $request['bicycle_type'],
            'bicycle_model' => $request ['bicycle_model'],
            'color' => $request['color'],
            'frame_size' => $request['frame_size'],
            'serial_number' => $request['serial_number'],
            'is_steal' => $request['is_steal'],

        ]);

        return back();
    }


    public function showGallery($id)
    {
        $bicycle = Bicycle::where('id', $id)->first();

        $images = Image::where('bicycle_id', $id)->get();

        if ((Auth::check() && Auth::user()))

            return view('bicycles.gallery', compact('bicycle', 'images'));

    }


    public function upload(Request $request, Bicycle $bicycle)
    {

        if ($request->hasFile('file')) {

            $request->input('file');

            $imageName = $request->file->HashName();

            $request->file->storeAs('public/uploads', $imageName);

        } else {

            return "Error!";

        }

        Image::create([

            'bicycle_id' => $bicycle->id,
            'path' => 'storage/uploads/' . $imageName,

        ]);


        return back();
    }

    public function info($id)
    {

        $bicycle = Bicycle::where('id', $id)->first();

        $user = User::find($id);

        return view('layouts.additional_information', compact('bicycle', 'user'));

    }

    public function edit($id)
    {

//        $bicycle = Bicycle::where('id', $id)->first();

        $bicycle = Bicycle::find($id);

        return view('bicycles.edit_page', compact('bicycle', 'id'));


    }

    public function update(Request $request, $id)
    {

        $bicycle = Bicycle::find($id);

        $this->validate(request(), [
            'bicycle_type' => 'required',
            'color' => 'required',
            'frame_size' => 'required|numeric',
            'serial_number' => 'required'

        ]);

        $bicycle->bicycle_manufacturer = $request->get('bicycle_manufacturer');
        $bicycle->bicycle_type = $request->get('bicycle_type');
        $bicycle->bicycle_model = $request->get('bicycle_model');
        $bicycle->color = $request->get('color');
        $bicycle->frame_size = $request->get('frame_size');
        $bicycle->serial_number = $request->get('serial_number');
        $bicycle->save();
        return redirect('bicycles');

    }

    public function destroy($id)
    {

        $bicycle = Bicycle::find($id);
        $bicycle->delete();

        return back();
    }

    public function steal_report(Request $request, $id)
    {

        $steal_report = StealReport::create([
            'user_id' => $request['user_id'],
            'bicycle_id' => $request['bicycle_id'],
            'bicycle_information' => $request['bicycle_information'],
            'steal_date' => $request['steal_date'],
            'steal_location' => $request['steal_location'],
            'steal_description' => $request['steal_description'],
            'is_read' => $request['is_read'],

        ]);

        StealReport::where('bicycle_id', $id)->first();
        Bicycle::where('id', $id)->update(['is_steal' => true]);

        event(new StolenBicycleAlertEvent($steal_report));

        return back();
    }

}

