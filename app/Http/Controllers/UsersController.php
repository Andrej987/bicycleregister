<?php

namespace App\Http\Controllers;

use App\Bicycle;
use App\Events\NewMessageReceived;
use App\Events\NewUserCreatedEvent;
use App\Events\NotificationsForUsersEvent;
use App\Message;
use App\Notifications;
use App\Repositories\Users;
use App\Role;
use App\StealReport;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('count', 'create', 'store', 'contact_us');
        $this->middleware('is_policeStation')->only('edit', 'destroy');
    }


    public function index(Request $request)
    {
        $users = User::all();

        $search = $request->input('search/users');

        $query = User::query();

        if ($search) {

            $query->where('surname', 'like', '%' . $search . '%')
                ->orWhere('residence', 'like', '%' . $search . '%')
                ->orWhere('OIB', 'like', '%' . $search . '%');

        }

        $users = $query->paginate(13);

        $admins = User::where('role_id', '2')->get();

        $notifications = Notifications::where('notification_role', 'newUser')->where('is_read', false)
            ->orderBy('created_at', 'desc')
            ->get();

        $messages = Notifications::where('notification_role', 'Message')->where('is_read', false)
            ->orderBy('created_at', 'desc')
            ->get();

        $steal_reports = Notifications::where('notification_role', 'Steal Report')->where('is_read', false)
            ->orderBy('created_at', 'desc')
            ->get();

        $new_notifications = Notifications::where('notification_role', 'newUser')->where('is_read', false)->count();
        $new_messages = Notifications::where('notification_role', 'Message')->where('is_read', false)->count();
        $new_steal_reports = Notifications::where('notification_role', 'Steal Report')->where('is_read', false)->count();

        return view('home', compact('users', 'admins', 'notifications', 'new_notifications', 'messages', 'new_messages', 'steal_reports', 'new_steal_reports'));
    }

    public function markAsRead()
    {
        Notifications::where('notification_role', 'newUser')->where('is_read', false)->update(['is_read' => true]);
        return back();
    }


    public function show_message($id)
    {

        Notifications::where('notification_role', 'Message')->where('message_id', $id)->where('is_read', false)->update(['is_read' => true]);
        $message = Message::find($id);
        return view('layouts.show_message', compact('message'));
    }

    public function show_steal_report($id)
    {

        Notifications::where('notification_role', 'Steal Report')->where('steal_report_id', $id)->where('is_read', false)->update(['is_read' => true]);
        StealReport::where('id', $id)->where('is_read', false)->update(['is_read' => true]);

        $steal_report = StealReport::find($id);
        $user = User::where('id', $steal_report->user_id)->first();
        $bicycle = Bicycle::where('id', $steal_report->bicycle_id)->first();
        return view('layouts.show_steal_report', compact('steal_report', 'user', 'bicycle'));
    }


    public function show($id, Bicycle $bicycle)
    {
        $user = User::find($id);
        if ((Auth::check() && Auth::user())) {

            return view('users.show', compact('user', 'bicycle'));

        } else {

            return back();
        }
    }


    public function create()
    {
        $users = User::where('role_id', '2')->get();
        $admin_id = \request('admin_id');

        return view('users.create', compact('users', 'admin_id'));

    }


    public function store()
    {
        $role = Role::where('name', 'user')->first();

        $user = User::create([
            'role_id' => $role->id,

            'admin_id' => Auth::user()->id,

            'name' => request('name'),
            'surname' => request('surname'),
            'email' => request('email'),
            'password' => Hash::make(request('password')),
            'residence' => request('residence'),
            'address'=> request('address'),
            'phone_number'=> request('phone_number'),
            'birth_date' => request('birth_date'),
            'OIB' => request('OIB')]);

        event(new NewUserCreatedEvent($user));

        return redirect('/home');

    }

    public function admin_create()
    {

        return view('users.admin_create');

    }

    public function admin_store()
    {

        $role = Role::where('name', 'admin')->first();

        User::create([
            'role_id' => $role->id,
            'name' => request('name'),
            'address'=> request('address'),
            'residence' => request('residence'),
            'email' => request('email'),
            'password' => Hash::make(request('password')),

        ]);

        return redirect('/home');

    }

    public function admin_dashboard($id)
    {
        $user = User::find($id);
        $users = User::where('admin_id', $user->id)->get();

        return view('users.admin_dashboard', compact('users', 'user'));
    }

    public function create_police_station(){

        return view('users.create_police_station');

    }

    public function police_station_store(){

        $role = Role::where('name', 'police_station')->first();

        User::create([
            'role_id' => $role->id,
            'name' => request('name'),
            'address'=> request('address'),
            'residence' => request('residence'),
            'email' => request('email'),
            'password' => Hash::make(request('password')),

        ]);

        return redirect('/home');

    }


    public function count()
    {

        $all_users = User::count();
        $all_bicycles = Bicycle::count();

        return view('welcome', compact('all_users', 'all_bicycles'));

    }

    public function edit($id)
    {
        //        $bicycle = Bicycle::where('id', $id)->first();
        $admins = User::where('role_id', '2')->get();
        $admin = User::find($id);
        $user = User::find($id);

        return view('users.edit_user', compact('user', 'admins', 'admin'));
    }

    public function update(Request $request, $id)
    {

        $user = User::find($id);

        $this->validate(request(), [
            'admin_id' => 'required',
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required',
            'residence' => 'required']);

        $user->admin_id = $request->get('admin_id');
        $user->name = $request->get('name');
        $user->surname = $request->get('surname');
        $user->email = $request->get('email');
        $user->residence = $request->get('residence');
        $user->save();
        return redirect('home');

    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return back();
    }

    public function contact_us()
    {
        $message = Message::create([
            'is_read' => false,
            'contact_name' => request('contact_name'),
            'email' => request('email'),
            'mobile_phone_number' => request('mobile_phone_number'),
            'contact_message' => request('contact_message')
        ]);

        event(new NewMessageReceived($message));
        return back();

    }

    public function notification_for_users(Request $request, User $user)
    {
        $notification_for_users = $request->get('notification_for_users');
        event(new NotificationsForUsersEvent($notification_for_users, $user));

    }
}
