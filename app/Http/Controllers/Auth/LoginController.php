<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\Users;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */


//    protected $redirectTo = 'home';

    public function authenticated(Request $request, $user)
    {

        if (Auth::user()->role_id == 3){

            return redirect('home');
        }


     elseif (Auth::user()->role_id == 2){

         return redirect('admins/' .Auth::user()->id);

     }

     elseif (Auth::user()->role_id == 1){

         return redirect('users/'.Auth::user()->id);

     }

        elseif (Auth::user()->role_id == 4){

            return redirect('home');

        }



    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
