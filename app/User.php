<?php

namespace App;

use App\Listeners\NewUserCreatedNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Debug\Exception\UndefinedFunctionException;

class User extends Authenticatable
{
    use Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'admin_id', 'name', 'surname', 'email', 'password', 'residence','address','phone_number', 'birth_date', 'OIB'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table = 'users';


    public function bicycles()
    {

        return $this->hasMany(Bicycle::class);

    }

    public function roles()
    {

        return $this->belongsTo(Role::class);

    }

//    public function users()
//    {
//
//        return $this->hasMany('User', 'admin_id');
//
//    }

    public function notifications()
    {

        return $this->hasMany(Notifications::class);

    }

//    public function messages()
//    {
//
//        return $this->hasMany(Message::class);
//
//    }

    public function steal_report()
    {

        return $this->hasMany(StealReport::class);

    }

}