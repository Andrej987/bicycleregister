<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Image extends Model
{

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bicycle_id', 'path'
    ];

    protected $table = 'images';


    public function bicycle(){

        $this->belongsTo(Bicycle::class);

    }

}
