<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    protected $fillable = [
   'notification_role', 'user_id','message_id', 'steal_report_id','text','is_read'
    ];

    protected $table = 'notifications';


    public function user(){

        return $this->belongsTo(User::class);

    }

    public function messages(){

        return $this->hasMany(Message::class);

    }

    public function steal_report(){

        return $this->hasMany(StealReport::class);

    }
}