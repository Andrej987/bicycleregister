<?php

namespace App\Listeners;

use App\Events\NewUserCreatedEvent;
use App\Notifications;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewUserCreatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserCreatedEvent  $event
     * @return void
     */
    public function handle(NewUserCreatedEvent $event)
    {
        $message = $event->user->name. '' . $event->user->surname. ' is just registered';
        $user_id = $event->user->id;

       Notifications::create([
           'notification_role'=> 'newUser',
           'user_id'=>$user_id,
           'message_id'=>null,
           'steal_report_id'=>null,
           'text'=> $message,
           'is_read'=> 0,

       ]);
    }
}
