<?php

namespace App\Listeners;

use App\Events\NewMessageReceived;
use App\Events\NewUserCreatedEvent;
use App\Message;
use App\Notifications;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class MessageReceivedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     *
     * @param NewMessageReceived $event
     * @return void
     */
    public function handle(NewMessageReceived $event)
    {
        $message = $event->message->contact_name . ' is just messaged you!';
        $message_id = $event->message->id;

        Notifications::create([
            'notification_role'=> 'Message',
            'user_id' => 0,
            'message_id'=>$message_id,
            'steal_report_id'=>null,
            'text' => $message,
            'is_read' => false,

        ]);
    }
}