<?php

namespace App\Listeners;

use App\Events\NewUserCreatedEvent;
use App\Events\NotificationsForUsersEvent;
use App\Notifications;


class NotificationsForUsersListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserCreatedEvent  $event
     * @return void
     */
    public function handle(NotificationsForUsersEvent $event)
    {
       $user = $event->user->id;
       $notification_for_users = $event->notification_for_users;

        Notifications::create([
            'notification_role'=> 'NtfForUsers',
            'user_id'=> $user,
            'message_id'=>null,
            'steal_report_id'=>null,
            'text'=> $notification_for_users,
            'is_read'=> 0,

        ]);

    }
}