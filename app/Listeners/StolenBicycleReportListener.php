<?php

namespace App\Listeners;

use App\Events\StolenBicycleAlertEvent;
use App\Notifications;

class StolenBicycleReportListener {

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     *
     *
     * @param StolenBicycleAlertEvent $event
     * @return void
     */
    public function handle(StolenBicycleAlertEvent $event){

        $stolen_bicycle = $event->steal_report -> bicycle_information . ' / is just reported as stolen! ';
        $user_id = $event->steal_report->user_id;
        $steal_report_id = $event->steal_report->id;

        Notifications::create([
            'notification_role'=> 'Steal Report',
            'user_id' => $user_id,
            'message_id'=>0,
            'steal_report_id'=>$steal_report_id,
            'text' => $stolen_bicycle,
            'is_read' => false,

        ]);
    }
}