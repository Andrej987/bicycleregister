<?php

namespace App\Repositories;

use App\User;

class Admins
{
    public function admins()
    {

        return User::where('role_id','2')->first();

    }
}